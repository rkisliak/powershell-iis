﻿cls
# Site Path
$sitePath = "C:\Site\"
$installError = $false
# We can install all/any or some features use list below
$features = @("IIS-WebServerRole","IIS-ManagementScriptingTools","IIS-IIS6ManagementCompatibility","IIS-LegacyScripts")
# Foreach all features
$features | %{
# Check if feature already enabled
    if(!(Get-WindowsOptionalFeature -Online -FeatureName $_)){
        try{
# If feature do not enabled => Enable it
            Enable-WindowsOptionalFeature -Online -All -NoRestart -FeatureName $_
        }
        catch
        {
# If any of feature failed write installError key
            Write-Host "Error within adding feature process."
            break
            $installError = $true
        }
    }
    else{
        Write-Host "Feature already installed."
    }
}
# If error key $false => them try to create IIS config.
if($installError -eq $false){
# Test path, if no folder => create
    if(!(Test-Path -Path $sitePath)){
        New-Item -Path $sitePath -ItemType Directory -Force | Out-Null
    }
# Check WAS and W3SVC services. If not started => Start.    
    if((Get-Service -Name WAS).Status -eq "Stopped"){
        Get-Service -Name WAS | Set-Service -StartupType Automatic
        Get-Service -Name WAS | Start-Service
    }
    if((Get-Service -Name W3SVC).Status -eq "Stopped"){
        Get-Service -Name W3SVC | Set-Service -StartupType Automatic
        Get-Service -Name W3SVC | Start-Service
    }
# Create AppPool    
    try {
        New-WebAppPool -Name SampleSiteAppPool -Force | Out-Null
        }
    catch {
        Write-Host "Error within Application Pool creation."
        break 
    }
# Create WebSite
    try{
        New-WebSite -Name SampleSite -Port 80 -HostHeader localhost -ApplicationPool SampleSiteAppPool -PhysicalPath $sitePath -Force | Out-Null
        }
    catch {
        Write-Host "Error within WebSite creation." 
        break
    }
# Start AppPool
    try{
        Start-WebAppPool -Name SampleSiteAppPool | Out-Null
        }
    catch {
        Write-Host "Can't start WebAppPool."
        break
    }
# Start Web-Site
    try{
        Start-Website -Name SampleSite | Out-Null
        }
    catch {
        Write-Host "Can't start WebSite."
        break
    }
   
# Get Acl => add IUSR => Set permissions to folder
    $folderPermissions = Get-Acl "C:\Site\"
    $folderPermissionsRule = New-Object System.Security.AccessControl.FileSystemAccessRule(“NT AUTHORITY\IUSR”,”FullControl”,“Allow”)
    $folderPermissions.AddAccessRule($folderPermissionsRule)
    try{
        Set-Acl "C:\Site\" $folderPermissions
        }
    catch{
        Write-Host "Can't assign NTFS folder permissions."
        break
    }
# Create index.html file. It can be copied from default website. But I created own.
    New-Item -Name index.html -Path $sitePath -ItemType File -Force | Out-Null
    "<H1>It works!</H1>" | Out-File -FilePath ($sitePath + "index.html") -Encoding utf8 -Force
# Start created website
    Start-Process -FilePath "c:\Program Files\Internet Explorer\iexplore.exe" -ArgumentList "http://localhost/"
}